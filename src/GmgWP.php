<?php

namespace GmgCore;

if (!class_exists("GmgWP")) {

    class GmgWP
    {
        public static function esc_meta($theID, $key=null){
            return esc_attr__(self::meta($theID, $key));
        }

        public static function meta($theID, $key=null, $isSingle=true)
        {
            return !is_string($key) ? get_post_meta($theID) : get_post_meta($theID, $key, $isSingle);
        }


    }
}
