<?php

namespace GmgCore;

class GmgModule
{

    public function __construct()
    {
    }

    public function initializeModule()
    {

    }


    public function advancedCustomFields()
    {
        return null;
    }

    public function basicMetaBoxes()
    {
        return null;
    }

    public function customPostTypes()
    {
        return null;
    }

    public function customTaxonomies()
    {
        return null;
    }

    public function shortCodes()
    {
        return null;
    }

}