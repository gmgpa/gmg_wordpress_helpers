<?php

namespace GmgCore;

//Add field groups action
/*
add_action('acf/init', 'gmg_custom_plugin_register_acf');
//register the field groups.
function gmg_custom_plugin_register_acf(){
*/
if (!class_exists("GmgAcfGen")) {

    class GmgAcfGen {


        private $field_group;

        public function __construct($field_group)
        {
            $this->field_group = $field_group;
        }

        public function run()
        {
            if(function_exists('acf_add_local_field_group')) {
                acf_add_local_field_group($this->field_group);
            }

        }

        /*
         *      2       222    22222
         *     2 2     2   2   2
         *    22222    2       2222
         *   2     2   2   2   2
         *  2       2   222    2
         *
         */

        public static function slp($params): array
        {
            return array(self::lp($params));
        }

        public static function lp($params): array
        {
            return array(
                'param' => $params[1] ?? 'post_type',
                'operator' => $params[2] ?? "==",
                'value' => $params[0],
            );
        }

        public static function locationParam($params, $single=false)
        {
            if ($single === false && !is_array($params[0])) {
                return array(self::lp($params));
            } elseif ($single === true && is_array($params[0])) {
    //            TODO: create and TEST a recursion that sets the location params
                $locations = array();
                foreach ($params as $locationParam => $param) {
                    $locationParam[] = self::lp($param);
                }
                return $locations;
            } else {
                return false;
            }
        }



        public function acf_field_group_generator($key, $title, $fields = [], $locations=[], $menu_order=0, $position='normal')
        {
            $field_group = array(
                'key' => 'group_5d6fefcd8964d',
                'title' => 'Block: GMG Form',
                'fields' => $fieldss,
                'location' => array(
                    array(
                        array(
                            'param' => 'block',
                            'operator' => '==',
                            'value' => 'acf/gmg-form',
                        ),
                    ),
                ),
                'menu_order' => 0,
                'position' => 'normal',
                'style' => 'default',
                'label_placement' => 'top',
                'instruction_placement' => 'label',
                'hide_on_screen' => '',
                'active' => true,
                'description' => '',
            );
            return $field_group;
        }

        public function field_generator()
        {

        }

        public static function date_picker_field()
        {
            return array(
                'key' => 'field_5e2b06b4c2cf1',
                'label' => 'Promo End Date',
                'name' => 'promo_end_date',
                'type' => 'date_picker',
                'instructions' => 'When does the promo end?',
                'required' => 1,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '50',
                    'class' => '',
                    'id' => '',
                ),
                'display_format' => 'F j, Y',
                'return_format' => 'F j, Y',
                'first_day' => 1,
            );
        }

        public static function post_type_field($postType=array(0=>'some_form')): array
        {
            return ['post_type' => $postType];
        }

        public static function extra_options_field()
        {
            $complete_field = [
                'conditional_logic' => 0,
                'taxonomy' => '',
                'allow_null' => 0,
                'multiple' => 0,
                'return_format' => 'id',
                'instructions' => '',
                'ui' => 1,
            ];
            return $complete_field;
        }


        /*
         * Field Options Array
         * @param:

         **/
        /**
         * @param $field_options
         * @param null $wrapper
         * 0: $key
         * 1: $label
         * 2: $name
         * 3: $type: [post_type, date]
         * 4: $required: bool
         * @param array $other_options
         * @return array
         */
        public static function base_field($field_options, $wrapper=null, $other_options=array())
        {
            $required = isset($field_options[4]) || isset($field_options['required']) ? 1 : 0;
            $my_field = [
                'key' => "field_".$field_options[0],
                'label' => $field_options[1],
                'name' => $field_options[2],
                'type' => $field_options[3],
                'required' => $required,
                'wrapper' => self::wrapperArr($wrapper),
            ];
            return array_merge($my_field,  $other_options);
        }

        const STANDARD_GMG_WRAPPER = [
            'width' => '',
            'class' => '',
            'id' => '',
        ];

        public static function wrapper($width='', $class='', $id='')
        {
            return array(
                'width' => $width,
                'class' => $class,
                'id' => $id,
            );
        }

        public static function wrapperArr($params=null): array
        {
            if (!is_null($params)) {
                if (is_string($params)) {
                    // Only uses the @width parameter
                    return self::wrapper($params);
                } elseif (is_array($params)) {
                    return array(
                        'width' => $params[0],
                        'class' => $params[1] ?? '',
                        'id' => $params[2] ?? '',
                    );
                }
            }

            return self::STANDARD_GMG_WRAPPER;
        }

        /**
         * @return array
         */
        public function getFieldGroup(): array
        {
            return $this->field_group;
        }


    }

}

