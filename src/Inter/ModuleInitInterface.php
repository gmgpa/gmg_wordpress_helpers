<?php

namespace GmgCore\Inter;

if (!interface_exists("ModuleInitInterface")) {
    interface ModuleInitInterface
    {

        public function getInitializationPosition();

    }
}
