<?php

namespace GmgCore;

if (!class_exists("GmgCore\QCPT")) {


    class QCPT extends \ArrayObject
    {
        const ORD_ASC = 'ASC';
        const ORD_DEC = 'DEC';//???
        const T = 'title';

        private $currentQuery = null;

        private $postQuery = [
            'posts_per_page' => '-1'
        ];

        public function __construct($postType) {
            $this->postQuery['post_type'] = $postType;
//            return $this;
        }

        public static function create($postType): QCPT
        {
            return new static($postType);
        }

        public function asc(): QCPT
        {
            $this->postQuery['order'] = static::ORD_ASC;
            return $this;
        }

        public function des(): QCPT
        {
            $this->postQuery['order'] = static::ORD_ASC;
            return $this;
        }

        public function postStatus($post_status='publish'): QCPT
        {
            $this->postQuery['post_status'] = $post_status;
            return $this;
        }

        public function orderby($orderby=null): QCPT
        {
            $this->postQuery['orderby'] = $orderby === null ? self::T : $orderby;
            return $this;
        }

        public function taxQuery($taxonomy, $field, $terms): QCPT
        {
            $tax_query = array(
                'taxonomy'  => $taxonomy,
                'field'     => $field,
                'terms'     => $terms
            );

            $this->postQuery['tax_query'] = array($tax_query);
            return $this;
        }

        public function metaQuery($key, $value, $compare="", $type=null): QCPT
        {
            // TYPES : CHAR
            // COMPARE : '=',
            $meta_query = array(
                'key'     => $key,
                'value'   => $value,
                'compare' => $compare,
            );

            if (is_string($type)) $meta_query['type'] = $type;

            $this->postQuery['meta_query'] = $meta_query;


            return $this;
        }

        public function getQuery(): \WP_Query
        {
            return new \WP_Query($this->postQuery);
        }

        public function myQuery()
        {
            return $this->currentQuery;
        }

        public function setQuery(): QCPT
        {
            $this->currentQuery = $this->getQuery();
            return $this;
        }

        public function resetQuery()
        {
            $this->currentQuery = null;
            wp_reset_postdata();
        }

        public static function oldWithPostsLoop($query)
        {
            if ( $query->have_posts() ) {
                while ( $query->have_posts() ) : $query->the_post();
                    $theID = get_the_ID();

                endwhile;
            }
        }

        public static function withPostsLoop($query, $callable, $noPostsMessage='No Posts Available')
        {
            if ($query->have_posts()) {
                while ( $query->have_posts() ) {
                    $query->the_post();
                    call_user_func($callable, $query);
                }

            } else {
                echo $noPostsMessage;
            }
        }

        public static function withPostsLoopReference($query, $callable, &$refArray, $noPostsMessage='No Posts Available')
        {
            if ($query->have_posts()) {
                while ( $query->have_posts() ) {
                    $query->the_post();
                    call_user_func($callable, $refArray);
                }

            } else {
                echo $noPostsMessage;
            }
        }



        public function offsetExists($offset): bool
        {
            return isset($this->postQuery[$offset]);
        }

        public function offsetGet($offset)
        {
            return $this->postQuery[$offset] ?? null;
        }

        public function offsetSet($offset, $value)
        {
            if (is_null($offset)) {
                $this->postQuery[] = $value;
            } else {
                $this->postQuery[$offset] = $value;
            }
        }

        public function offsetUnset($offset)
        {
            unset($this->postQuery[$offset]);
        }
    }

}
