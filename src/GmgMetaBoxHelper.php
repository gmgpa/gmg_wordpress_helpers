<?php

namespace GmgCore;


if (!class_exists("GmgMetaBoxHelper")) {


    /**
     * Generated by the WordPress Meta Box Generator at http://goo.gl/8nwllb
     */
    class GmgMetaBoxHelper {

        //Change this metabox to title each one you have
        const SCREEN = 'gmg-meta-box';

        const CHOICE_TYPES = ['radio', 'checkbox', 'select'];


        protected $post = null;

    //    FROM STICKY???
        private $fields = array(
            array(
                'id' => 'message_text',
                'label' => 'Message Text',
                'type' => 'textarea',
            ),
            array(
                'id' => 'start_date',
                'label' => 'Start Date',
                'type' => 'date',
            ),
            array(
                'id' => 'end_date',
                'label' => 'End Date',
                'type' => 'date',
            ),
            array(
                'id' => 'top_or_bottom',
                'label' => 'Top or Bottom',
                'type' => 'radio',
                'options' => array(
                    'Top',
                    'Bottom',
                ),
            ),
            array(
                'id' => 'background_color',
                'label' => 'Background Color',
                'type' => 'radio',
                'options' => array(
                    'White',
                    'Black',
                ),
            ),
            array(
                'id' => 'font_color',
                'label' => 'Font Color',
                'type' => 'radio',
                'options' => array(
                    'White',
                    'Black',
                    'Red',
                    'Blue',
                    'Green',
                ),
            ),
            array(
                'id' => 'background_transparency',
                'label' => 'Background Transparency (%)',
                'type' => 'number',
            ),
        );


        /**
         * Class construct method. Adds actions to their respective WordPress hooks.
         */
        public function __construct() {
            add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ) );
            add_action( 'save_post', array( $this, 'save_post' ) );
        }

        public function createFields()
        {

        }

        protected static function metabox_field($id, $type, $options=[], $label_description=null)
        {
    //        Turns the ID into readable instructions, add
            $label = ucwords(str_replace(['-', '_'], ' ', $id));
            $field = array(
                'id' => $id,
                'type' => $type
            );
            if (in_array($type, static::CHOICE_TYPES)) {

            }

            return $field;
        }

        /**
         * Hooks into WordPress' add_meta_boxes function.
         * Goes through screens (post types) and adds the meta box.
         */
        public function add_meta_boxes(){

            global $post;

            if( get_post_type( $post->ID ) == static::SCREEN ){
    //      if( $post->post_name == 'sticky-message' ){
    //      error_log( 'Post slug is ' . $post->post_name );
                add_meta_box(
                    'sticky-message-meta-box',
                    __( 'Sticky Message Meta Box', 'rational-metabox' ),
                    array( $this, 'add_meta_box_callback' ),
                    static::SCREEN,
                    'advanced',
                    'high'
                );
            }
        }

        /**
         * Generates the HTML for the meta box
         *
         * @param object $post WordPress post object
         */
        public function add_meta_box_callback( $post ) {
            wp_nonce_field( 'sticky_message_meta_box_data', 'sticky_message_meta_box_nonce' );
            $this->generate_fields( $post );
        }

        /**
         * Generates the field's HTML for the meta box.
         */
        public function generate_fields( $post ) {
            $output = '';
            foreach ( $this->fields as $field ) {
                $label = '<label for="' . $field['id'] . '">' . $field['label'] . '</label>';
                $db_value = get_post_meta( $post->ID, 'sticky_message_meta_box_' . $field['id'], true );
                switch ( $field['type'] ) {
                    case 'textarea':
                        $input = sprintf(
                            '<textarea class="large-text" id="%s" name="%s" rows="5">%s</textarea>',
                            $field['id'],
                            $field['id'],
                            $db_value
                        );
                        break;
                    case 'radio':
                        $input = '<fieldset>';
                        $input .= '<legend class="screen-reader-text">' . $field['label'] . '</legend>';
                        $i = 0;
                        foreach ( $field['options'] as $key => $value ) {
                            $field_value = !is_numeric( $key ) ? $key : $value;
                            $input .= sprintf(
                                '<label><input %s id="%s" name="%s" type="radio" value="%s"> %s</label>%s',
                                $db_value === $field_value ? 'checked' : '',
                                $field['id'],
                                $field['id'],
                                $field_value,
                                $value,
                                $i < count( $field['options'] ) - 1 ? '<br>' : ''
                            );
                            $i++;
                        }
                        $input .= '</fieldset>';
                        break;
                    default:
                        $input = sprintf(
                            '<input %s id="%s" name="%s" type="%s" value="%s">',
                            $field['type'] !== 'color' ? 'class="regular-text"' : '',
                            $field['id'],
                            $field['id'],
                            $field['type'],
                            $db_value
                        );
                }
                $output .= $this->row_format( $label, $input );
            }
            echo '<table class="form-table"><tbody>' . $output . '</tbody></table>';
        }

        /**
         * Generates the HTML for table rows.
         */
        public function row_format( $label, $input ) {
            return sprintf(
                '<tr><th scope="row">%s</th><td>%s</td></tr>',
                $label,
                $input
            );
        }
        /**
         * Hooks into WordPress' save_post function
         */
        public function save_post( $post_id ) {

            if( get_post_type( $post_id ) == 'gmg-stickys'){

                if ( ! isset( $_POST['sticky_message_meta_box_nonce'] ) )
                    return $post_id;

                $nonce = $_POST['sticky_message_meta_box_nonce'];
                if ( !wp_verify_nonce( $nonce, 'sticky_message_meta_box_data' ) )
                    return $post_id;

                if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
                    return $post_id;

                foreach ( $this->fields as $field ) {
                    if ( isset( $_POST[ $field['id'] ] ) ) {
                        switch ( $field['type'] ) {
                            case 'email':
                                $_POST[ $field['id'] ] = sanitize_email( $_POST[ $field['id'] ] );
                                break;
                            case 'text':
                                $_POST[ $field['id'] ] = sanitize_text_field( $_POST[ $field['id'] ] );
                                break;
                        }
                        update_post_meta( $post_id, 'sticky_message_meta_box_' . $field['id'], $_POST[ $field['id'] ] );
                    } else if ( $field['type'] === 'checkbox' ) {
                        update_post_meta( $post_id, 'sticky_message_meta_box_' . $field['id'], '0' );
                    }
                }
            }
        }
    }

}

