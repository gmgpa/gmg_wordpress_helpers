<?php

namespace GmgCore;

class GmgHelp {



//    public static function queryArgs($post_type, $name=null, $orderBy='title',
//                                     $taxQuery=array(), $order='ASC', $postsPerPage='-1',
//                                     $postStatus=null)
//    {
//        $queryArr = array(
//            'post_type'              => $post_type,
//            'posts_per_page'         => $postsPerPage,
//        );
//
//        if (!is_string($order)) {$queryArr['orderby'] = $orderBy;}
//
//        if ($order) {$queryArr['order'] = $order;}
//
//        if (is_string($name)) {$queryArr['title'] = $name;}
//
//        if (!empty($taxQuery)) {$queryArr['tax_query'] = array($taxQuery);}
//
//        if ($postStatus) {
//            $queryArr['post_status'] = $postStatus;
//        }
//
//        return $queryArr;
//    }
//
//    public static function queryArgDefault($post_type, $postStatus=null, $postsPerPage='-1', $name=null, $orderBy='title',
//                                     $taxQuery=array(), $order='ASC')
//    {
//
//        return self::queryArgs($post_type, $name, $orderBy, $taxQuery, $order, $postsPerPage, $postStatus);
//    }

//    public static function withPostsLoop($query, $callable, $noPostsMessage='No Posts Available')
//    {
//        if ($query->have_posts()) {
//            while ( $query->have_posts() ) {
//                $query->the_post();
//                call_user_func($callable, $query);
//            }
//
//        } else {
//            echo $noPostsMessage;
//        }
//    }

    public static function fix_phone( $phone ){
//        error_log('Fix phone is ' . $phone );
        $phone_numbs = strpos($phone, '-') != false ? preg_replace("/[^0-9]/", "", $phone) : $phone;

        if ( strlen($phone_numbs) == 10 ){
//            error_log('Phone is ' . $phone_numbs );
            preg_match( "/([0-9]{3})([0-9]{3})([0-9]{4})/", $phone_numbs, $phone_pieces );
            $phone = $phone_pieces[1] . '-' . $phone_pieces[2] . '-' . $phone_pieces[3];
        } else {
            preg_match( "/([0-9]{1})([0-9]{3})([0-9]{3})([0-9]{4})/", $phone_numbs, $phone_pieces );
            $phone = $phone_pieces[1] . '-' . $phone_pieces[2] . '-' . $phone_pieces[3] . '-' . $phone_pieces[4];
        }
//        error_log('Fix Phone is now ' . $phone );
        return $phone;

    }

    public static function getStateAbbr(): array
    {

        return array('GA', 'AL', 'AK', 'AS', 'AZ', 'AR', 'CA', 'CO', 'CT', 'DE', 'DC', 'FM', 'FL', 'GU', 'HI', 'ID', 'IL', 'IN', 'IA', 'KS', 'KY', 'LA', 'ME', 'MH', 'MD', 'MA', 'MI', 'MN', 'MS', 'MO', 'MT', 'NE', 'NV', 'NH', 'NJ', 'NM', 'NY', 'NC', 'ND', 'MP', 'OH', 'OK', 'OR', 'PW', 'PA', 'PR', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VT', 'VI', 'VA', 'WA', 'WV', 'WI', 'WY', 'AE', 'AA', 'AP');
    }

    public static function pageExistsBySlug( $post_slug ): bool
    {
        $args_posts = array(
            'post_type'      => array ( 'page' ),
            'post_status'    => array ( 'publish' ),
            'name'           => $post_slug,
            'posts_per_page' => 1,
        );

        return (bool)get_posts($args_posts);
    }

}