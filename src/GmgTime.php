<?php

namespace GmgCore;

class GmgTime {

    const ABBR_LOWER_DAYS = array(
        0 => 'mon',
        1 => 'tues',
        2 => 'wed',
        3 => 'thurs',
        4 => 'fri',
        5 => 'sat',
        6 => 'sun'
    );
    
    private $start_day;
    
    private $end_day;
    
    private $start_time;
    
    private $end_time;
    
    public function __construct( $start_day, $end_day, $start_time, $end_time ) {
        $this->start_day = $start_day;
        $this->end_day = $end_day;
        $this->start_time = $start_time;
        $this->end_time = $end_time;
    }
        
    /*Get Days*/
    
    //Get Start Day    
    public function get_start_day() {
        return $this->start_day;
    }
    
    //Get End Day    
    public function get_end_day() {
        return $this->end_day;
    }
    
    
    //Get Start Time    
    public function get_start_time() {
        return $this->start_time;
    }
    
    
    //Get End Time    
    public function get_end_time() {
        return $this->end_time;
    }
    
    
    /*Set Days*/
    
    //Set Start Day    
    public function set_start_day( $start_day ) {
        $this->start_day = $start_day;
    }
    
    //Set End Day    
    public function set_end_day( $end_day ) {
        $this->end_day = $end_day;
    }
    
    
    //Set Start Time    
    public function set_start_time( $start_time ) {
        $this->start_time = $start_time;
    }
    
    
    //Set End Time    
    public function set_end_time( $end_time ) {
        $this->end_time = $end_time;
    }    
}
