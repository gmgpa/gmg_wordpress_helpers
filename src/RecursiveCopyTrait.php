<?php

namespace GmgCore;

if (!trait_exists("RecursiveCopyTrait")) {

    trait RecursiveCopyTrait
    {

        public static function recurseCopy($src,$dst, $level=0)
        {
            if($src !== '') {
                $dir = opendir($src);
                if(!file_exists($dst) ){
                    mkdir( $dst );
                }

                while(false !== ( $file = readdir($dir)) ) {
                    if (( $file !== "." ) && ( $file != ".." )) {
                        $srcFile = $src . '/' . $file;
                        if ( is_dir($srcFile) ) {
                            self::recurseCopy($srcFile, $dst . '/' . $file, $level);
                        } else {
                            copy($srcFile,$dst . '/' . $file);
                        }
                    }
                }
                closedir($dir);
            }
        }

    }
}
